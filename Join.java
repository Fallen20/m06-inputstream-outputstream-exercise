//Montse Gordillo Cumplido - 2WIAM
package ejercicio2_Contenido;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class Join {

	public static void main(String[] args) throws IOException {
	
		Path carpeta= Paths.get("C:\\Users\\monts\\Desktop\\aa");//ruta carpeta general
		Path archivoFinal= carpeta.resolve(carpeta+"\\a.txt");//archivo todo junto
		
		int numArchivo=0;
		Path archivosSeparados= carpeta.resolve(carpeta+"\\a.txt.part."+numArchivo);//partes
		boolean existe=Files.exists(archivosSeparados);//si existe la parte0
		
		InputStream lectura = Files.newInputStream(archivosSeparados);//esto lee el primero
		OutputStream escribir = Files.newOutputStream(archivoFinal);//esto escribe
		int leido;
		
		for(;;numArchivo++) {
			archivosSeparados= carpeta.resolve(carpeta+"\\a.txt.part."+numArchivo);//crea el archivo de nuevo
			existe=Files.exists(archivosSeparados);//mira si existe, reset cada vez que miras otro archivo
			
			
			if(existe==true) {//si existe, haz esto
				lectura = Files.newInputStream(archivosSeparados);
				//lee de nuevo, si lo pones fuera peta porque intenta buscar el siguiente num. Solo debe leer si existe
				leido = lectura.read();//se puede obviar esta var con escribir.write(lectura.read());
				escribir.write(leido);
				}
			else {break;}//importate porque sino existe se queda sin poder entrar a ningun lado y es infinito
			
		}
		
		
		
		
		
	}

}
